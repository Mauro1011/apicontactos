package com.example.consumirws;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class GetAnimalesAsyncTask extends AsyncTask<Void, Void, Void> {

    private String url;

    public GetAnimalesAsyncTask(String url)
    {
        this.url = url;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try {
            URL uri = new URL(url);
            HttpURLConnection conexion = (HttpURLConnection) uri.openConnection();
            conexion.setRequestMethod("GET");
            InputStream entradaDatos = conexion.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(entradaDatos));
            StringBuilder respuesta = new StringBuilder();
            String linea="";
            while ((linea = reader.readLine()) != null)
                respuesta.append(linea);

            System.out.println(respuesta.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
