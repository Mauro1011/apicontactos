package com.example.consumirws;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView listAnimales;
    private Button botonActualizar;
    private ArrayAdapter<String> adaptador;
    private ArrayList<String> animales = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listAnimales = findViewById(R.id.listanimales);
        botonActualizar = findViewById(R.id.btn);
        botonActualizar.setOnClickListener(this);
        adaptador = new ArrayAdapter(this,android.R.layout.simple_list_item_1,animales);
        listAnimales.setAdapter(adaptador);

        new GetAnimalesAsyncTask("http://192.168.42.39/serviceContactos/api/Contactos").execute();
    }

    @Override
    public void onClick(View v) {

    }
}
